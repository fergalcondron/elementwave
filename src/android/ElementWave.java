package com.elementwave;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * This class echoes a string called from JavaScript.
 */
public class ElementWave extends CordovaPlugin
{
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException
    {
        if (action.equals("init"))
        {
            String userKey = args.getString(0);
            String appKey = args.getString(1);
            String gcmProjNumber = args.getString(2);

            this.init(userKey, appKey, gcmProjNumber, callbackContext);
            return true;
        }
        
        if (action.equals("pauseActivity"))
        {
            this.pauseActivity(callbackContext);
        }
        
        if (action.equals("resumeActivity"))
        {
            this.resumeActivity(callbackContext);
        }

        if (action.equals("addTag"))
        {
            String tagName = args.getString(0);
            this.addTag(tagName, callbackContext);
        }

        if (action.equals("addTagWithID"))
        {
            String tagName = args.getString(0);
            String tagId = args.getString(1);

            this.addTagWithID(tagName, tagId, callbackContext);
        }

        if (action.equals("removeTag"))
        {
            String tagName = args.getString(0);
            this.removeTag(tagName, callbackContext);
        }

        return false;
    }

    private void init(String userKey, String appKey, String gcmProjNumber, CallbackContext callbackContext)
    {
        if ( (appKey != null && appKey.length() > 0) &&
             (userKey != null && userKey.length() > 0) )
        {
            new ie.elementsoftware.ElementWave(cordova.getActivity(), gcmProjNumber, appKey, userKey);
            callbackContext.success("It Works !!" + gcmProjNumber);
        }
        else
        {
            callbackContext.error("Expected User Key and App Key arguments.");
        }
    }

    private void addTag(String tagName, CallbackContext callbackContext)
    {
        if (tagName != null && tagName.length() > 0)
        {
            ie.elementsoftware.ElementWave.addTag(tagName, null, cordova.getActivity().getApplicationContext());
            callbackContext.success("Added Tag");
        }
        else
        {
            callbackContext.error("Expected Tag Name.");
        }
    }

    private void addTagWithID(String tagName, String tagID, CallbackContext callbackContext)
    {
        if ( (tagName != null && tagName.length() > 0) &&
             (tagID != null) && (tagID.length() > 0) )
        {
            ie.elementsoftware.ElementWave.addTag(tagName, tagID, cordova.getActivity().getApplicationContext());
            callbackContext.success("Added Tag With ID");
        }
        else
        {
            callbackContext.error("Expected Tag Name and Tag ID.");
        }
    }

    private void removeTag(String tagName, CallbackContext callbackContext)
    {
        if (tagName != null && tagName.length() > 0)
        {
            ie.elementsoftware.ElementWave.removeTag(tagName, cordova.getActivity().getApplicationContext());
            callbackContext.success("Removed Tag.");
        }
        else
        {
            callbackContext.error("Expected Tag Name.");
        }
    }

    private void pauseActivity(CallbackContext callbackContext)
    {
        ie.elementsoftware.ElementWave.onPause(cordova.getActivity().getApplicationContext());
        callbackContext.success("Pause");
    }
    
    private void resumeActivity(CallbackContext callbackContext)
    {
        ie.elementsoftware.ElementWave.onResume(cordova.getActivity().getApplicationContext());
        callbackContext.success("Resume");
    }
}