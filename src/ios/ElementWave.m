
/********* Echo.m Cordova Plugin Implementation *******/

#import "ElementWave.h"
#import <Cordova/CDV.h>
#import "ElementWavePush.h"

@implementation ElementWave

- (void) init:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;

    if ([command.arguments count] < 3)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }
    
    NSString *userKey = [command.arguments objectAtIndex:0];
    NSString *appKey = [command.arguments objectAtIndex:1];
    NSString *gcmProjNumber = [command.arguments objectAtIndex:2];
    //NSString *androidContext = [command.arguments objectAtIndex:3];

    if ( ([userKey length] == 0) || ([appKey length] == 0) )
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }

    [[ElementWavePush sharedObject] launchWithUserKey: userKey andAppKey: appKey];

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Success"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];    
}

- (void) addTag:(CDVInvokedUrlCommand *) command
{
    CDVPluginResult* pluginResult = nil;

    if ([command.arguments count] < 1)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }

    NSString *tagName = [command.arguments objectAtIndex:0];
    [[ElementWavePush sharedObject] addTag:tagName];

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Success"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];    
}

- (void) addTagWithIdentifier:(CDVInvokedUrlCommand *) command
{
    CDVPluginResult* pluginResult = nil;

    if ([command.arguments count] < 2)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }

    NSString *tagName = [command.arguments objectAtIndex:0];
    NSString *tagID = [command.arguments objectAtIndex:1];

    [[ElementWavePush sharedObject] addTag: tagName withID: tagID];

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Success"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];    
}

- (void) removeTag:(CDVInvokedUrlCommand *) command
{
    CDVPluginResult* pluginResult = nil;

    if ([command.arguments count] < 1)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }

    NSString *tagName = [command.arguments objectAtIndex:0];
    [[ElementWavePush sharedObject] removeTag:tagName];

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Success"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];    
}

- (void) enableLocation:(CDVInvokedUrlCommand *) command
{
    [[ElementWavePush sharedObject] setLocationEnabled:YES];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) disableLocation:(CDVInvokedUrlCommand *) command
{
    [[ElementWavePush sharedObject] setLocationEnabled:NO];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}

- (void) enableBackgroundLocation:(CDVInvokedUrlCommand *) command
{
    [[ElementWavePush sharedObject] setBackgroundLocationEnabled:YES];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) disableBackgroundLocation:(CDVInvokedUrlCommand *) command
{
    [[ElementWavePush sharedObject] setBackgroundLocationEnabled:NO];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) setSandboxMode:(CDVInvokedUrlCommand *) command
{
    [[ElementWavePush sharedObject] useSandboxMode];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end

