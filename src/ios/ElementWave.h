#import <Cordova/CDV.h>

@interface ElementWave : CDVPlugin

- (void) init:(CDVInvokedUrlCommand *) command;
- (void) addTag:(CDVInvokedUrlCommand *) command;
- (void) addTagWithIdentifier:(CDVInvokedUrlCommand *) command;
- (void) removeTag:(CDVInvokedUrlCommand *) command;
- (void) setSandboxMode:(CDVInvokedUrlCommand *) command;

- (void) enableLocation:(CDVInvokedUrlCommand *) command;
- (void) disableLocation:(CDVInvokedUrlCommand *) command;
- (void) enableBackgroundLocation:(CDVInvokedUrlCommand *) command;
- (void) disableBackgroundLocation:(CDVInvokedUrlCommand *) command;

@end
