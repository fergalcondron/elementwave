//
//  ElementWavePush.h
//  ElementWavePush
//
//  Version 2.52
//
//  Created by James Harkin on 26/02/2013.
//  Copyright (c) 2013 Element Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

// Definitions
typedef enum
{
    AppIsRunning = 1,
    AppIsRunningInBackground,
    AppIsNotRunning
} AppStatus;


@interface ElementWavePush : NSObject

// Methods
+ (ElementWavePush *) sharedObject;
- (void) launchWithUserKey:(NSString *) theUserKey andAppKey:(NSString *) theAppKey;
- (NSString *) deviceToken;

// Use the APNS Sandbox server instead of the main APNS production server
- (void) useSandboxMode;

// Push Notifications
- (void) setPushNotificationsEnabled:(BOOL) status;

//Location service
- (void) setLocationEnabled:(BOOL) status;
- (void) setBackgroundLocationEnabled:(BOOL) status;

// Tags
- (void) addTag:(NSString *) theTag;
- (void) addTag:(NSString *) theTag withID:(NSString *) tagID;
- (void) removeTag:(NSString *) theTag;
- (void) removeAllTags;

// Username
- (void) setUsername:(NSString *) theUsername;

@end
