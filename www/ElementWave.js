
var ElementWave = function() {};

ElementWave.prototype.init = function(userKey, appKey, gcmProjNumber, callback)
{
cordova.exec(callback, function(err)
        {
            callback('Element Wave could not be initialised.');
        },
        "ElementWave", "init", [userKey, appKey, gcmProjNumber]);
}

ElementWave.prototype.addTag = function(tagName, callback)
{
cordova.exec(callback, function(err)
        {
            callback('Element Wave could not add tag.');
        },
        "ElementWave", "addTag", [tagName]);
}

ElementWave.prototype.addTagWithID = function(tagName, tagID, callback)
{
cordova.exec(callback, function(err)
        {
            callback('Element Wave could not add tag with ID.');
        },
        "ElementWave", "addTagWithID", [tagName, tagID]);
}

ElementWave.prototype.removeTag = function(tagName, callback)
{
cordova.exec(callback, function(err)
        {
            callback('Element Wave could not remove tag.');
        },
        "ElementWave", "removeTag", [tagName]);
}

ElementWave.prototype.enableLocation = function(callback)
{
cordova.exec(callback, function(err)
        {
            callback('Element Wave could not enable location.');
        },
        "ElementWave", "enableLocation", []);
}

ElementWave.prototype.disableLocation = function(callback)
{
cordova.exec(callback, function(err)
        {
            callback('Element Wave could not disable location.');
        },
        "ElementWave", "disableLocation", []);
}

ElementWave.prototype.enableBackgroundLocation = function(callback)
{
cordova.exec(callback, function(err)
        {
            callback('Element Wave could not enable background location.');
        },
        "ElementWave", "enableBackgroundLocation", []);
}

ElementWave.prototype.disableBackgroundLocation = function(callback)
{
cordova.exec(callback, function(err)
        {
            callback('Element Wave could not disable background location.');
        },
        "ElementWave", "disableBackgroundLocation", []);
}

ElementWave.prototype.setSandboxMode = function(callback)
{
cordova.exec(callback, function(err)
        {
            callback('Element Wave could not enable sandbox mode.');
        },
        "ElementWave", "setSandboxMode", []);
}


ElementWave.prototype.pause = function(callback)
{
cordova.exec(callback, function(err)
        {
            callback('Could not pause activity');
        },
        "ElementWave", "pauseActivity", []);
}

ElementWave.prototype.resume = function(callback)
{
cordova.exec(callback, function(err)
        {
            callback('Could not resume activity');
        },
        "ElementWave", "resumeActivity", []);
}

              
var elementWave = new ElementWave();
module.exports = elementWave;
